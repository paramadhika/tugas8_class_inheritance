//Class Utama
open class Laptop(_namaLaptop: String, _prosesor: String, _ram: String, _penyimpanan: String, _os: String, _harga: Int){
    open val namalaptop = _namaLaptop
    open val prosesor = _prosesor
    open val ram = _ram
    open val storage = _penyimpanan
    open val sistemOperasi = _os
    open val harga = _harga

    //Cetak Informasi Spesifikasi Laptop
    open fun cetakSpek() : String{
        val spekLaptop = """
            
            =========[ Spesifikasi Laptop $namalaptop ]=========
                        Nama Laptop     : $namalaptop
                        Prosesor        : $prosesor
                        RAM             : $ram GB
                        Penyimpanan     : $storage GB
                        OS              : $sistemOperasi
                        Harga           : Rp.$harga
                        
        """.trimIndent()
        return spekLaptop
    }
}
// Class Anak yang digunakan untuk mencetak spesifikasi laptop Asus
class Asus(_namaLaptop : String, _prosesor : String, _ram : String, _penyimpanan: String, _os : String, _harga: Int) : Laptop(_namaLaptop, _prosesor, _ram, _penyimpanan, _os, _harga){
    override fun cetakSpek(): String {
        return super.cetakSpek()
    }
}

fun main() {
    val laptopAsus = listOf(Asus("Asus VivoBook 14 A413", "Intel Core I7-1165G7", "8", "SSD 512", "Windows 10 Home x64", 13499000),
                            Asus("Asus VivoBook A509MA", "Intel Celeron 4305U", "4", "SSD 256","Windows 10 Home x64", 5099000),
                            Asus("Asus VivoBook FLIP 14 TM420", "AMD Ryzen 7 4700U", "8", "SSD 512","Windows 10 Home x64", 9499000)
                            )
    for(i in laptopAsus){
        println(i.cetakSpek())
    }
}